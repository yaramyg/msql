package com.Goutham.MySql.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.Goutham.MySql.model.Employee;



public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}